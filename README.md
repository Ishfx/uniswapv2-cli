# UniswapV2 basic CLI

Get prices of ERC20 tokens from listed pairs in UniswapV2.

## Lib documentation
[https://ishfx.gitlab.io/uniswapv2-cli](https://ishfx.gitlab.io/uniswapv2-cli)

## Getting started

### Binary
Download the binaries from the releases : [UniswapV2 releases](https://gitlab.com/Ishfx/uniswapv2-cli/-/releases)

Tested on :
- Linux
- Wine on Linux
- Windows 10

__The MacOS binaries were NOT tested.__

### Docker
A platform agnostic solution is to use Docker.

```sh
docker pull registry.gitlab.com/ishfx/uniswapv2-cli:latest
```

## Usage

### Docker
```sh
docker run --rm -t registry.gitlab.com/ishfx/uniswapv2-cli:latest --help
```

### Binary
```sh
> cd /path/to/binary
> chmod +x ./uniswapv2-cli # only for unix systems
> ./uniswapv2-cli --help
```

### First usage

```md
> ./uniswapv2-cli --help

Usage: uniswapv2-cli [options]

Options:
  -h, --help       Show help                                           [boolean]
  -V, --version    Show version number                                 [boolean]
  -v, --verbose    Show debug logs                                     [boolean]
  -m, --machine    Output result directly to stdout for machines       [boolean]
  -n, --network    EVM compatible network id (1, 3, 56, etc)
                                                           [number] [default: 1]
  -k, --infurakey  Infura key for eth node provider, if not set a default
                   provider will be used                                [string]
  -a, --amount     Source token amount                     [number] [default: 1]
  -s, --stoken     Source token symbol or address     [string] [default: "WETH"]
  -d, --dtoken     Destination token symbol or address [string] [default: "DAI"]

Examples:
  uniswapv2-cli -a 1 -s WETH -d DAI  Returns the maximum output amount of DAI for 
                              1ETH (fees included)
```

### Query a price for WETH/WBTC pair

#### Solution 1: Using addresses (prefered solution)
```sh
> ./uniswapv2-cli -a 1 -s 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2 -d 0x2260fac5e5542a773aa44fbcfedf7c193bc2c599
2022-04-09T12:39:02.877 info UniswapV2: 1 of WETH = 0.07533746 of WBTC (fees included) 
```

#### Solution 2: Using symbols (works only for mainnet)
```sh
> ./uniswapv2-cli -a 1 -s WETH -d WBTC
2022-04-09T12:36:35.394 info UniswapV2: 1 of WETH = 0.07533746 of WBTC (fees included) 
```

### Pipe the result
```sh
> ./uniswapv2-cli -a 1 -s WETH -d WBTC -m | xargs printf "%.3f\n"
0,075
```

## Developpement

### Prerequisites
- node.js
- yarn or npm

### Prepare developpement env
```sh
> git clone https://gitlab.com/Ishfx/uniswapv2-cli
> cd ./uniswapv2-cli
> yarn install
```

### Run in watch mode
```sh
> yarn start:dev
```