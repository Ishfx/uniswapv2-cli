# ------------------------------------------------------------------------------------------------ #
#                                            Build stage                                           #
# ------------------------------------------------------------------------------------------------ #
FROM node:17-alpine as builder

WORKDIR /build

COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock

RUN yarn install --frozen-lockfile

COPY . .

RUN yarn babel:build && \
    yarn package -t alpine-x64

# ------------------------------------------------------------------------------------------------ #
#                                           Runtime stage                                          #
# ------------------------------------------------------------------------------------------------ #
FROM alpine:3.15

WORKDIR /app

COPY --from=builder /build/dist-bin/uniswapv2-cli ./uniswapv2-cli

ENTRYPOINT [ "/app/uniswapv2-cli" ]