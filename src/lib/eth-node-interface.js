import { getNetwork } from '@ethersproject/networks';
import { InfuraProvider, JsonRpcProvider } from '@ethersproject/providers';

/**
 * Supported ETH networks ids
 */
export const NetworkChainId = {
    MAINNET: 1,
    ROPSTEN: 3,
    RINKEBY: 4,
    GÖRLI: 5,
    KOVAN: 42,
};

const DEFAULT_PROVIDER = 'https://cloudflare-eth.com/';

/**
 * Defines an interface to communicate with blockchain through an ETH node
 * @module EthNodeInterface
 */
export default class EthNodeInterface {
    /**
     * ETH interface provider
     */
    get provider() {
        return this._provider;
    }

    /**
     * Initialize a new instance of ethereum node interface to exchange data with the blockhain
     *
     * @param {number} networkChainId Network chain id (mainnet, ropsten, etc), if not set MAINNET
     * will be user
     * @param {string} infuraPrivateKey infra private key, if not set, a default provider will be
     * used
     */
    constructor(networkChainId, infuraPrivateKey, logger) {
        const chainId = networkChainId || NetworkChainId.MAINNET;

        let _provider = null;
        const network = getNetwork(chainId);

        if (infuraPrivateKey) {
            logger.debug(`Using infura provider on network ${network.name}`);
            _provider = new InfuraProvider(network, infuraPrivateKey);
        } else {
            logger.debug(`Using default provider (${DEFAULT_PROVIDER}) on network ${network.name}`);
            _provider = new JsonRpcProvider(DEFAULT_PROVIDER, network);
        }

        this._provider = _provider;
    }
}
