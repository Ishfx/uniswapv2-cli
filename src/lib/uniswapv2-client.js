import { getCreate2Address } from '@ethersproject/address';
import { pack, keccak256 } from '@ethersproject/solidity';
import { UniswapV2PairContract, UniswapV2Router02Contract } from './contracts';

// see uniswap docs for these addresses
const UNISWAPV2_FACTORY = '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f';
const INIT_CODE_HASH = '0x96e8ac4277198ff8b6f785478aa9a39f403cb768dd02cbee326c3e7da348845f';

/**
 * A basic client to communicate with uniswap V2 contracts
 * @module UniswapV2Client
 */
export default class UniswapV2Client {
    /**
     * Computes address of a given pair offchain
     * Based on : [getting-pair-addresses](https://docs.uniswap.org/sdk/2.0.0/guides/getting-pair-addresses)
     *
     * @param {Token} token0 token 0 data
     * @param {Token} token1 token 1 data
     * @returns token0/token1 pair address
     */
    static computePairAddress(token0, token1) {
        // tokens must be sorted by sort order.
        const tokens = token0.isLessThan(token1)
            ? [token0.address, token1.address]
            : [token1.address, token0.address];

        return getCreate2Address(
            UNISWAPV2_FACTORY,
            keccak256(['bytes'], [pack(['address', 'address'], tokens)]),
            INIT_CODE_HASH
        );
    }

    /**
     * Initialize a new instance of Uniswap client to communicate with uniswap contracts
     *
     * @param {BaseProvider} provider Eth network interface provider
     * @param {Logger} logger Generic logger
     */
    constructor(provider, logger) {
        this.provider = provider;
        this.logger = logger;
    }

    /**
     * Get reserve of token pair
     *
     * [UniswapV2 getreserves](https://docs.uniswap.org/protocol/V2/reference/smart-contracts/pair#getreserves)
     *
     * @param {Token} token0 token 0 data
     * @param {Token} token1 token 1 data
     * @returns Uniswap reserves of token pair in the same order as input
     */
    async getPairReserves(token0, token1) {
        const pairAddress = UniswapV2Client.computePairAddress(token0, token1);

        this.logger.debug(`Found pair address of ${token0}/${token1}: ${pairAddress}`);

        const contract = new UniswapV2PairContract(pairAddress, this.provider);
        const { reserve1: rsrv1, reserve2: rsrv2, timestamp } = await contract.getReserves();

        this.logger.debug(`Got reserves for pair ${token0}/${token1}: ${rsrv1},${rsrv2}`);

        const token0First = token0.isLessThan(token1);
        const reserve1 = token0First ? rsrv1 : rsrv2;
        const reserve2 = token0First ? rsrv2 : rsrv1;

        return { reserve1, reserve2, timestamp };
    }

    /**
     * Get maximum output token amount for a given input token amount (fees included).
     * Onchain computation
     *
     * [UniswapV2 getamountout](https://docs.uniswap.org/protocol/V2/reference/smart-contracts/library#getamountout)
     *
     * @param {BigNumber} tokenSourceAmount Amount of token to swap
     * @param {Token} tokenSource Input token data
     * @param {Token} tokenDestination Output token data
     * @returns {BigNumber} Maximum amount of output token
     */
    async getTokenAmountOutOnChain(tokenSourceAmount, tokenSource, tokenDestination) {
        const { reserve1: reserveSource, reserve2: reserveDestination } =
            await this.getPairReserves(tokenSource, tokenDestination);

        const contract = new UniswapV2Router02Contract(this.provider);
        const amount = await contract.getAmountOut(
            tokenSourceAmount,
            reserveSource,
            reserveDestination
        );

        this.logger.debug(
            `Got amount out onchain: ${tokenSourceAmount}${tokenSource}=${amount}${tokenDestination}`
        );

        return amount;
    }

    /**
     * Get maximum output token amount for a given input token amount (fees included).
     * Offchain computation
     *
     * ``` md
     * k = x(t-1) * y(t-1) = x(t) * y(t)
     * y(t) = y(t-1) - tDest
     * x(t) = x(t-1) + tSrcF
     * x(t-1) * y(t-1) = (x(t-1) + tSrcF) * (y(t-1) - tDest)
     * x(t-1) * y(t-1) = x(t-1) * y(t-1) - x(t-1) * tDest + tSrcF * y(t-1) - tSrcF * tDest
     * tDest(x(t-1) + tSrcF) = tSrcF * y(t-1)
     * tDest = (tSrcF * y(t-1)) / (tSrcF + x(t-1))
     *
     * tSrcF = tSrc - tSrc*0.003 = 0.997*tSrc
     * ```
     *
     *
     *
     * @param {BigNumber} tokenSourceAmount Amount of token to swap
     * @param {Token} tokenSource Input token data
     * @param {Token} tokenDestination Output token data
     * @returns {BigNumber} Maximum amount of output token
     */
    async getTokenAmountOutOffChain(tokenSourceAmount, tokenSource, tokenDestination) {
        const { reserve1: reserveSource, reserve2: reserveDestination } =
            await this.getPairReserves(tokenSource, tokenDestination);

        const sourceAmountWithFees = tokenSourceAmount.mul(997).div(1000); // 997/1000 = 0.997
        const nominator = sourceAmountWithFees.mul(reserveDestination);
        const denominator = reserveSource.add(sourceAmountWithFees);
        const amount = nominator.div(denominator);

        this.logger.debug(
            `Got amount out offchain: ${tokenSourceAmount}${tokenSource}=${amount}${tokenDestination}`
        );

        return amount;
    }
}
