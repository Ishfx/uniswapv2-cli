import { UniswapV2Router02Abi } from './abi';
import AbstractContract from './abstract-contract';

/**
 * Represents a uniswap v2 router2 contract
 * @module UniswapV2Router02Contract
 */
export default class UniswapV2Router02Contract extends AbstractContract {
    /**
     * Initialize a new instance of uniswap v2 router2 contract to communicate with it
     * @param {BaseProvider} provider ETH interface provider
     */
    constructor(provider) {
        super('0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', UniswapV2Router02Abi, provider);
    }

    /**
     * Given an input amount of an asset and pair reserves, returns the maximum output amount of
     * the other asset
     */
    async getAmountOut(amountIn, reserveIn, reserveOut) {
        return this.contract.getAmountOut(amountIn, reserveIn, reserveOut);
    }

    // other contract methods go here
}
