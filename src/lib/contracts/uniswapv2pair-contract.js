import { UniswapV2PairAbi } from './abi';
import AbstractContract from './abstract-contract';

/**
 * Represents a uniswap v2 pair contract
 * @module UniswapV2PairContract
 */
export default class UniswapV2PairContract extends AbstractContract {
    /**
     * Initialize a new instance of uniswap v2 pair contract to communicate with it
     * @param {string} address contract address
     * @param {BaseProvider} provider ETH interface provider
     */
    constructor(address, provider) {
        super(address, UniswapV2PairAbi, provider);
    }

    /**
     * Returns reserves of both tokens in this pair
     */
    async getReserves() {
        const [reserve1, reserve2, timestamp] = await this.contract.getReserves();
        return { reserve1, reserve2, timestamp };
    }

    // other contract methods go here
}
