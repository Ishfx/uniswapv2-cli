import { ERC20Abi } from './abi';
import AbstractContract from './abstract-contract';

/**
 * Represents an ERC20 contract
 * @module ERC20Contract
 */
export default class ERC20Contract extends AbstractContract {
    /**
     * Initialize a new instance of ERC20 contract to communicate with it
     * @param {string} address ERC20 token address
     * @param {BaseProvider} provider ETH interface provider
     */
    constructor(address, provider) {
        super(address, ERC20Abi, provider);
    }

    /**
     * Returns the name of the token.
     */
    async name() {
        return this.contract.name();
    }

    /**
     * Returns the symbol of the token, usually a shorter version of the name
     */
    async symbol() {
        return this.contract.symbol();
    }

    /**
     * Returns the number of decimals used to get its user representation.
     */
    async decimals() {
        return this.contract.decimals();
    }

    // other contract methods go here
}
