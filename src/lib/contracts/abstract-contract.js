import { Contract } from '@ethersproject/contracts';

/**
 * Abstract class for contracts
 * @module AbstractContract
 */
export default class AbstractContract {
    /**
     * Contract instance
     */
    get contract() {
        return this._contract;
    }

    /**
     * Abstract constructor of AbstractContract, should not be called directly
     *
     * @param {string} address Contract address
     * @param {object} abi Contract ABI
     * @param {BaseProvider} provider ETH interface provider
     */
    constructor(address, abi, provider) {
        if (!address) throw new Error('contract address not defined');
        if (!abi) throw new Error('contract abi not defined');
        if (!provider) throw new Error('provider not defined');

        this._contract = new Contract(address, abi, provider);
    }
}
