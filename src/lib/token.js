import { getAddress } from '@ethersproject/address';
import { getTokenInfo } from 'erc20-token-list';
import { ERC20Contract } from './contracts';
import { NetworkChainId } from './eth-node-interface';

/**
 * Represents an ERC20 token
 * @module Token
 */
export default class Token {
    /**
     * Returns an instance of token for the specified address
     *
     * @param {string} address token address
     * @param {BaseProvider} provider ETH interface provider
     * @param {NetworkChainId} chainId Network chain id
     * @returns {Promise<Token>} A new instance of Token
     */
    static async fromAddress(address, provider, chainId) {
        const contract = new ERC20Contract(address, provider);
        const decimals = await contract.decimals();
        const symbol = await contract.symbol();
        const name = await contract.name();
        return new Token(chainId, address, decimals, name, symbol);
    }

    /**
     * Returns an instance of token for the specified symbol
     * CAVEAT: only works for MAINNET
     *
     * @param {string} symbol token symbol
     * @returns {Token} A new instance of Token
     */
    static fromSymbol(symbol) {
        const token = getTokenInfo(symbol);

        if (!token)
            throw new Error(`ERC20 token ${symbol} not found, try directly using its address`);

        const { address, decimals, name } = token;
        return new Token(NetworkChainId.MAINNET, address, decimals, name, symbol);
    }

    /**
     * Initialize a new instance of ERC20 token
     *
     * @param {number} chainId Network chain id
     * @param {string} address Token address
     * @param {number} decimals Number of decimal for amount
     * @param {string} symbol A string representation for token
     */
    constructor(chainId, address, decimals, name, symbol) {
        try {
            this.address = getAddress(address);
        } catch (error) {
            throw new Error(`${address} is not a valid address.`);
        }

        this.chainId = chainId || NetworkChainId.MAINNET;
        this.decimals = decimals || 18;
        this.name = name || '';
        this.symbol = symbol || '';
    }

    equals(token) {
        return this.address === token.address && this.chainId === token.chainId;
    }

    /**
     * Format token to string
     *
     * @returns {string} string representation of token
     */
    toString() {
        return `${this.name} (${this.symbol})`;
    }

    /**
     * Returns true if the address of this token is strictly less than the given token by sort order
     *
     * @param {Token} token token to compare
     * @returns {boolean} true if less, false otherwise
     */
    isLessThan(token) {
        if (this.equals(token)) throw new Error('Cant compare similar tokens');
        return this.address.toLowerCase() < token.address.toLowerCase();
    }
}
