import { BigNumber } from '@ethersproject/bignumber';
import { Decimal } from 'decimal.js';

/**
 * Converts number (float) to big number
 *
 * @param {number} number input number
 * @param {number} decimals number of decimals
 * @returns input * 10^decimals
 */
export const toBigNumber = (number, decimals) => {
    const scale = new Decimal(10).pow(decimals);
    const value = new Decimal(number);
    return BigNumber.from(value.mul(scale).toString());
};

/**
 * Converts input to decimal
 *
 * @param {BigNumber} bigNumber input big number
 * @param {number} decimals number of decimals
 * @returns input / 10^decimals
 */
export const toDecimal = (bigNumber, decimals) => {
    const scale = new Decimal(10).pow(decimals);
    const value = new Decimal(bigNumber.toString());
    return value.div(scale);
};
