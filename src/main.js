import logger from 'npmlog';
import yargs from 'yargs/yargs';
import { hideBin } from 'yargs/helpers';
import { version } from '../package.json';

import {
    EthNodeInterface,
    UniswapV2Client,
    Token,
    NetworkChainId,
    toBigNumber,
    toDecimal,
} from './lib';

/* ---------------------------------------------------------------------------------------------- */
/*                                           Application                                          */
/* ---------------------------------------------------------------------------------------------- */
/**
 * Program application
 * @module App
 */
class App {
    constructor() {
        this.createLogger();
    }

    /**
     * Create program logger
     */
    createLogger() {
        const tzOffset = new Date().getTimezoneOffset() * 60000; // offset in milliseconds
        const now = () => new Date(Date.now() - tzOffset).toISOString().slice(0, -1);

        Object.defineProperty(logger, 'heading', { get: () => now() });
        logger.headingStyle = { bg: '', fg: 'white' };
        // eslint-disable-next-line no-console
        logger.raw = (message) => console.log(message || '');
        logger.debug = (message) => logger.verbose(message || '');
    }

    /**
     * Parse and checks program arguments
     *
     * @param {string[]} rawArgs Program arguments
     * @returns Parsed arguments
     */
    parseArgs(rawArgs) {
        return yargs(hideBin(rawArgs))
            .locale('en')
            .usage('Usage: $0 [options]')
            .example(
                '$0 -a 1 -s WETH -d DAI',
                'Returns the maximum output amount of DAI for 1ETH (fees included)'
            )

            .version(false)
            .option('version', {
                alias: 'V',
                type: 'boolean',
                description: 'Show version number',
            })

            .option('verbose', {
                alias: 'v',
                type: 'boolean',
                description: 'Show debug logs',
            })

            .option('machine', {
                alias: 'm',
                type: 'boolean',
                description: 'Output result directly to stdout for machines',
            })

            .option('network', {
                alias: 'n',
                type: 'number',
                description: 'EVM compatible network id (1, 3, 56, etc)',
                default: 1,
            })

            .option('infurakey', {
                alias: 'k',
                type: 'string',
                description:
                    'Infura key for eth node provider, if not set a default provider will be used',
            })

            .option('amount', {
                alias: 'a',
                description: 'Source token amount',
                type: 'number',
                default: 1,
            })

            .option('stoken', {
                alias: 's',
                description: 'Source token symbol or address',
                type: 'string',
                default: 'WETH',
            })

            .option('dtoken', {
                alias: 'd',
                description: 'Destination token symbol or address',
                type: 'string',
                default: 'DAI',
            })

            .option('help', {
                alias: 'h',
                type: 'boolean',
                description: 'Show help',
            }).argv;
    }

    /**
     * Print version
     *
     * @returns program exit code
     */
    printVersion() {
        logger.info(`Uniswap basic CLI - version ${version}`);
        logger.info(`Licence MIT (${new Date().getFullYear()})`);

        return 0;
    }

    /**
     * Runs the default application
     *
     * @param {Object} args Parsed arguments
     * @returns program exit code
     */
    async runApp({ network, infurakey, amount, stoken, dtoken, machine }) {
        const sourceTokenIsAddress = stoken.toLowerCase().startsWith('0x');
        const destinationTokenIsAddress = dtoken.toLowerCase().startsWith('0x');

        // arguments check
        if (network !== NetworkChainId.MAINNET) {
            if (!infurakey)
                throw new Error(
                    'Please provide an infura key when using this network. Default provider is only available for mainnet.'
                );

            if (!sourceTokenIsAddress || !destinationTokenIsAddress)
                throw new Error(
                    'Please provide token addresses (hex format) when using this network.'
                );
        }

        // lets go
        const ethNodeInterface = new EthNodeInterface(network, infurakey, logger);

        const sourceToken = sourceTokenIsAddress
            ? await Token.fromAddress(stoken, ethNodeInterface.provider)
            : Token.fromSymbol(stoken);
        const destinationToken = destinationTokenIsAddress
            ? await Token.fromAddress(dtoken, ethNodeInterface.provider)
            : Token.fromSymbol(dtoken);

        if (sourceToken.equals(destinationToken))
            throw new Error('Source and destination token should be different');

        logger.debug(`Src token info: ${sourceToken}`);
        logger.debug(`Dst token info: ${destinationToken}`);

        const uniswapClient = new UniswapV2Client(ethNodeInterface.provider, logger);

        const amountOut = await uniswapClient.getTokenAmountOutOffChain(
            toBigNumber(amount, sourceToken.decimals),
            sourceToken,
            destinationToken
        );

        const decimalAmount = toDecimal(amountOut, destinationToken.decimals);

        if (machine) logger.raw(decimalAmount);
        else
            logger.info(
                `UniswapV2: ${amount} of ${sourceToken.symbol} = ${decimalAmount} of ${destinationToken.symbol} (fees included)`
            );

        return 0;
    }

    /**
     * Entrypoint of the application
     * @param {string[]} rawArgs Program arguments
     */
    async run(rawArgs) {
        let exitCode = 0;

        const args = this.parseArgs(rawArgs);
        logger.level = args.machine ? 'silent' : args.verbose ? 'verbose' : 'info';

        try {
            if (args.version) exitCode = this.printVersion();
            else exitCode = await this.runApp(args);
        } catch (error) {
            logger.error(args.verbose ? error.stack : `An error has occured: ${error.message}.`);
            exitCode = 1;
        }

        process.exit(exitCode);
    }
}

/* ---------------------------------------------------------------------------------------------- */
/*                                           Entrypoint                                           */
/* ---------------------------------------------------------------------------------------------- */
/**
 * Main entrypoint
 */
async function main() {
    await new App().run(process.argv);
}

main();
