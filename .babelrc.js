const generateAliases = (jsconfig) => {
    const aliases = {};
    const { paths } = jsconfig.compilerOptions;
    for (const [alias, path] of Object.entries(paths))
        aliases[alias.replace('/*', '')] = path[0].replace('./', './src/').replace('/*', '');

    return aliases;
};

module.exports = {
    presets: [['@babel/preset-env', { targets: { node: '12' } }]],
    plugins: [
        [
            'module-resolver',
            {
                alias: generateAliases(require('./jsconfig.json')),
            },
        ],
        '@babel/plugin-proposal-export-default-from',
    ],
};
