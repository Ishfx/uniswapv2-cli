'use strict';

const { compilerOptions } = require('./jsconfig.json');

const generateAliases = (jsconfigPaths) => {
    const aliases = {};
    for (const [alias, path] of Object.entries(jsconfigPaths)) {
        const key = `^${alias.replace('/*', '(.*)$')}`;
        const value = `<rootDir>/${path[0].replace('./', 'src/').replace('/*', '$1')}`;
        aliases[key] = value;
    }

    return aliases;
};

module.exports = {
    moduleNameMapper: generateAliases(compilerOptions.paths),
    collectCoverageFrom: ['src/**/*.js', '!src/main.js'],
    verbose: true,
};
