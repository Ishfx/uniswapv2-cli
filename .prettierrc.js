module.exports = {
    printWidth: 100,

    overrides: [
        {
            files: '*.js',
            options: {
                tabWidth: 4,
                singleQuote: true,
                quoteProps: 'consistent',
            },
        },
        {
            files: '*.json',
            options: {
                tabWidth: 2,
            },
        },
    ],
};
