import { toBigNumber, toDecimal } from '@lib';
import { BigNumber } from '@ethersproject/bignumber';

describe('Utils', () => {
    it('converts to BigNumber', () => {
        const n = toBigNumber(123, 18);
        expect(n).toBeInstanceOf(BigNumber);
        expect(() => n.toNumber()).toThrow();
        expect(n.toString()).toBe('123000000000000000000');
    });

    it('converts to Decimal', () => {
        const n = toDecimal(BigNumber.from('123000000000000000000'), 18);
        expect(n.toNumber()).toBe(123);
    });
});
