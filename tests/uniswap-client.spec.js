import { EthNodeInterface, NetworkChainId, Token, UniswapV2Client } from '@lib';
import { BigNumber } from '@ethersproject/bignumber';

describe('UniswapV2 client', () => {
    const { provider } = new EthNodeInterface(NetworkChainId.MAINNET, null, console);
    const token1 = Token.fromSymbol('WETH');
    const token2 = Token.fromSymbol('WBTC');
    const pair = '0xBb2b8038a1640196FbE3e38816F3e67Cba72D940';
    const client = new UniswapV2Client(provider, console);

    it('computes pair address', async () => {
        expect(UniswapV2Client.computePairAddress(token1, token2)).toBe(pair);
    });

    it('returns pair reserves', async () => {
        const { reserve1, reserve2, timestamp } = await client.getPairReserves(token1, token2);
        expect(reserve1).toBeInstanceOf(BigNumber);
        expect(reserve2).toBeInstanceOf(BigNumber);
        expect(timestamp).toBeGreaterThanOrEqual(0);
    });

    it('returns amount out - onchain', async () => {
        const amountOut = await client.getTokenAmountOutOnChain(
            BigNumber.from(`1${'0'.repeat(18)}`),
            token1,
            token2
        );

        expect(amountOut).toBeInstanceOf(BigNumber);
    });

    it('returns amount out - offchain', async () => {
        const amountOut = await client.getTokenAmountOutOffChain(
            BigNumber.from(`1${'0'.repeat(18)}`),
            token1,
            token2
        );

        expect(amountOut).toBeInstanceOf(BigNumber);
    });

    it('amount out onchain-offchain are equal', async () => {
        const amountOut1 = await client.getTokenAmountOutOnChain(
            BigNumber.from(`1${'0'.repeat(18)}`),
            token1,
            token2
        );

        const amountOut2 = await client.getTokenAmountOutOffChain(
            BigNumber.from(`1${'0'.repeat(18)}`),
            token1,
            token2
        );

        expect(amountOut1).toStrictEqual(amountOut2);
    });
});
