import { EthNodeInterface, NetworkChainId } from '@lib';
import { ERC20Contract } from '@lib/contracts';

describe('ERC20 token contract', () => {
    let contract = null;
    const { provider } = new EthNodeInterface(NetworkChainId.MAINNET, null, console);
    const address = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599';

    beforeAll(() => {
        contract = new ERC20Contract(address, provider);
    });

    it('initializes', () => {
        expect(contract).toBeDefined();
    });

    it('returns ERC20 token name', async () => {
        expect(await contract.name()).toBe('Wrapped BTC');
    });

    it('returns ERC20 token symbol', async () => {
        expect(await contract.symbol()).toBe('WBTC');
    });

    it('returns ERC20 token decimals', async () => {
        expect(await contract.decimals()).toBe(8);
    });
});
