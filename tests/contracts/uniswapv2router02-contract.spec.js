import { EthNodeInterface, NetworkChainId } from '@lib';
import { UniswapV2Router02Contract } from '@lib/contracts';

describe('UniswapV2 Router2 contract', () => {
    let contract = null;
    const { provider } = new EthNodeInterface(NetworkChainId.MAINNET, null, console);

    beforeAll(() => {
        contract = new UniswapV2Router02Contract(provider);
    });

    it('initializes', () => {
        expect(contract).toBeDefined();
    });

    it('returns amount out', async () => {
        const amountOut = await contract.getAmountOut(10, 100, 1000);
        expect(amountOut.toNumber()).toBe(90);
    });
});
