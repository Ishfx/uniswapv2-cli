import { EthNodeInterface, NetworkChainId } from '@lib';
import { UniswapV2PairContract } from '@lib/contracts';
import { BigNumber } from '@ethersproject/bignumber';

describe('UniswapV2 Pair contract', () => {
    let contract = null;
    const { provider } = new EthNodeInterface(NetworkChainId.MAINNET, null, console);
    const address = '0xb4e16d0168e52d35cacd2c6185b44281ec28c9dc';

    beforeAll(() => {
        contract = new UniswapV2PairContract(address, provider);
    });

    it('initializes', () => {
        expect(contract).toBeDefined();
    });

    it('returns USDC/WETH reserves', async () => {
        const { reserve1, reserve2, timestamp } = await contract.getReserves();
        expect(reserve1).toBeInstanceOf(BigNumber);
        expect(reserve2).toBeInstanceOf(BigNumber);
        expect(timestamp).toBeGreaterThanOrEqual(0);
    });
});
