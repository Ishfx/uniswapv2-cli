import { EthNodeInterface, NetworkChainId, Token } from '@lib';

describe('Token', () => {
    const { provider } = new EthNodeInterface(NetworkChainId.MAINNET, null, console);

    it('initializes from address', async () => {
        const token = await Token.fromAddress(
            '0x6b175474e89094c44da98b954eedeac495271d0f',
            provider
        );

        expect(token.name).toBe('Dai Stablecoin');
        expect(token.symbol).toBe('DAI');
        expect(token.decimals).toBe(18);
        expect(token.address).toBe('0x6B175474E89094C44Da98b954EedeAC495271d0F');
    });

    it('initializes from symbol', () => {
        const token = Token.fromSymbol('DAI');

        expect(token.name).toBe('Dai Stablecoin v2.0');
        expect(token.symbol).toBe('DAI');
        expect(token.decimals).toBe(18);
        expect(token.address).toBe('0x6B175474E89094C44Da98b954EedeAC495271d0F');
    });

    it('checks for equality', async () => {
        const token1 = await Token.fromAddress(
            '0x6b175474e89094c44da98b954eedeac495271d0f',
            provider
        );

        const token2 = Token.fromSymbol('DAI');

        expect(token1.equals(token2)).toBeTruthy();
    });

    it('compares', () => {
        const token1 = Token.fromSymbol('WETH');
        const token2 = Token.fromSymbol('USDT');

        expect(token1.isLessThan(token2)).toBeTruthy();
    });
});
