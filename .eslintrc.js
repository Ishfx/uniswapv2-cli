const generateAliases = (jsconfig) => {
    const aliases = [];
    const { paths } = jsconfig.compilerOptions;
    for (const [alias, path] of Object.entries(paths)) {
        const mapping = [];
        mapping[0] = alias.replace('/*', '');
        mapping[1] = path[0].replace('./', './src/').replace('/*', '');
        aliases.push(mapping);
    }

    return aliases;
};

module.exports = {
    env: {
        es6: true,
        node: true,
    },
    extends: ['airbnb-base', 'prettier'],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module',
    },
    settings: {
        'import/resolver': {
            node: { paths: ['./src'] },
            alias: generateAliases(require('./jsconfig.json')),
        },
    },
    rules: {
        'no-plusplus': 'off',
        'no-nested-ternary': 'off',
        'no-restricted-syntax': 'off',
        'no-underscore-dangle': 'off',
        'no-return-assign': ['error', 'except-parens'],
        'class-methods-use-this': 'off',
    },
};
